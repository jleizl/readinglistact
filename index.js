
// Activity Template:
// (Copy this part on your template)


/*
	1.) Create a function that returns a passed string with letters in alphabetical order.
		Example string: 'mastermind'
		Expected output: 'adeimmnrst'
	
*/

// Code here:

console.log("#1 output")

function alphabet_order(str)
  {
return str.split('').sort().join('');
  }
console.log(alphabet_order("incomprehensibilities"));

/*
	2.) Write a simple JavaScript program to join all elements of the following array into a string.

	Sample array : myColor = ["Red", "Green", "White", "Black"];
		Expected output: 
			Red,Green,White,Black
			Red,Green,White,Black
			Red+Green+White+Black

*/

// Code here:

console.log("")
console.log("#2 output")

myFavColor = ["Pink", "Gray", "White", "Black"];
console.log(myFavColor.toString());
console.log(myFavColor.join());
console.log(myFavColor.join('+'));

/*
	3.) Write a function named birthdayGift that pass a gift as an argument.
		- if the gift is a stuffed toy, return a message that says: "Thank you for the stuffed toy, Michael!"
		- if the gift is a doll, return a message that says: "Thank you for the doll, Sarah!"
		- if the gift is a cake, return a message that says: "Thank you for the cake, Donna!"
		- if other gifts return a message that says: "Thank you for the (gift), Dad!"
		- create a global variable named myGift and invoke the function in the variable.
		- log the global var in the console

*/

// Code here:

console.log("")
console.log("#3 output")

let myGift=""

function birthdayGift(gift){

	if (gift == "stuffed toy") {
		return 'Thank you for the stuffed toy, Michael!';
	} else if (gift == "doll") {
		return "Thank you for the doll, Sarah!";
	} else if (gift == "cake") {
		return 'Thank you for the cake, Donna!'
	} else {
		return "Thank you for the " + gift + ", Dad!";
	}
}


myGift = birthdayGift("stuffed toy");
console.log(myGift)

myGift = birthdayGift("doll");
console.log(myGift)

myGift = birthdayGift("cake");
console.log(myGift)

myGift = birthdayGift("book");
console.log(myGift)

/*
	4.) Write a function that accepts a string as a parameter and counts the number of vowels within the string.

		Example string: 'The quick brown fox'
		Expected Output: 5

*/

// Code here:

console.log("")
console.log("#4 output")

function vowel_count(str1)
{
  var vowel_list = 'aeiouAEIOU';
  var vcount = 0;
  
  for(var x = 0; x < str1.length ; x++)
  {
    if (vowel_list.indexOf(str1[x]) !== -1)
    {
      vcount += 1;
    }
  
  }
  return vcount;
}
console.log(vowel_count("Hello, World! it’s time code."));